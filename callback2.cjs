function listInformationBasedOnBoardID(boardId,callbackfn){
    setTimeout(()=>{
        const listsInfo = require('./lists_1.json');
        const list = listsInfo[boardId];
        if(!list){
            callbackfn(new Error(`List not found for the boardId ${boardId}`),null);
            return;
        }
        callbackfn(null,list);
       
   },4000);
}
module.exports = listInformationBasedOnBoardID;