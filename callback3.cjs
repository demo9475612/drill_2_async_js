function  allCardsThatBelongToParticularListBasedOnListID(listID,callback){
    setTimeout(() => {
        const cards = require('./cards.json');
        const listCards = cards[listID];
        if (!listCards) {
            callback(new Error(`Cards for list with ID ${listID} not found`), null);
        } else {
            callback(null, listCards);
        }
    }, 6000);
}
module.exports =allCardsThatBelongToParticularListBasedOnListID;