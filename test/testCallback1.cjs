const boardsInformationBasedOnBoardID= require('../callback1.cjs');

try{
 
    const boardId = 'mcu453ed';
    boardsInformationBasedOnBoardID(boardId.trim(),(error,boardInfo)=>{
        if(error){
            console.error("Error:",error);
            return;
        }
        console.log("Board is:",boardInfo);
    })

}catch(error){
    console.error(error);
}

module.exports = boardsInformationBasedOnBoardID;