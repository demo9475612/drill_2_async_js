const  allCardsThatBelongToParticularListBasedOnListID =require("../callback3.cjs");

try{
    const listId = 'qwsa221';
    allCardsThatBelongToParticularListBasedOnListID(listId.trim(),(error,cardInfo)=>{
        if(error){
            console.error("Error:",error);
            return;
        }
        console.log("All cards that belong to a particular list based on the listID is:",cardInfo);
    });

}catch(error){
    console.error(error);
}