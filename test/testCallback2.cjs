const listInformationBasedOnBoardID = require("../callback2.cjs");

try{
    const boardId = 'mcu453ed';
    listInformationBasedOnBoardID(boardId.trim(),(error,listInfo)=>{
        if(error){
            console.error("Error:",error);
            return;
        }
        console.log("Lists that belong to a board based on the boardID is:",listInfo);
    });

}catch(error){
    console.error(error);
}