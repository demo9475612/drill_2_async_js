

function boardsInformationBasedOnBoardID(boardId,callbackfn){

       setTimeout(()=>{
            const boardsInfo = require('./boards.json');
            const board=boardsInfo.find(board=>board.id===boardId);
            //console.log(board);
            if(!board){
                callbackfn(new Error(`Board with boardId ${boardId} is not found in boardsInfo`),null);
            }else{
                callbackfn(null,board);
            }
            
           
       },2000);

    
}
module.exports = boardsInformationBasedOnBoardID;